#!/usr/bin/env python3

class animals:

  def __init__(self, age, height, weight, legs, colour, furORskin):
    self.age = 10
    self.height = 25
    self.weight = 5
    self.legs = 4
    self.colour = "grey"
    self.furORskin = "fur"

  def eat(self):
    self.weight +=0.5
    self.height +=0.5

  def run(self):
    self.weight -=0.1

  def birthday(self):
    self.age +=1
